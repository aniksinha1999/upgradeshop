import "./App.css";
import Navbar from "./components/Navbar";

import LoginComponent from "./components/LoginComponent";
import { Routes, Route } from "react-router-dom";

import SignUpComponent from "./components/SignUpComponent";
import ProductsPage from "./components/ProductsPage";
import Address from "./components/Address";
import ConfirmOrderPage from "./components/ConfirmOrderPage";
import CreateProductForAdmin from "./components/CreateProductForAdmin";
import OrderComponent from "./components/OrderComponent";
import OrderStepper from "./components/OrderAddressSummaryStepper";

function App() {
  return (
    <div className="App">
      <Navbar />

      <Routes>
        <Route path="/login" element={<LoginComponent />} />{" "}
        {/* Route for LoginComponent */}
        <Route path="/signup" element={<SignUpComponent />} />{" "}
        {/* Route for SignupComponent */}
        <Route path="/product" element={<ProductsPage />} />{" "}
        {/* Route for SignupComponent */}
        <Route path="/address" element={<Address />} />{" "}
        {/* Route for SignupComponent */}
        <Route path="/summary" element={<ConfirmOrderPage />} />{" "}
        {/* Route for SignupComponent */}
        <Route path="/addProduct" element={<CreateProductForAdmin />} />{" "}
        {/* Route for SignupComponent */}
        <Route path="/order" element={<OrderComponent />} />{" "}
        {/* Route for SignupComponent */}
        <Route path="/orderprocess" element={<OrderStepper />} />{" "}
        {/* Route for SignupComponent */}
      </Routes>
    </div>
  );
}

export default App;
