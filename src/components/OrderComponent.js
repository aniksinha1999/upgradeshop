import {
  Box,
  Container,
  Grid,
  Card,
  CardActions,
  CardMedia,
  Typography,
  Button,
  CardContent,
} from "@mui/material";
export default function OrderComponent() {
  return (
    <Box>
      <Container>
        <Grid lg={6}>
          <Card sx={{ maxWidth: 345, mt: 8, ml: 3 }}>
            <CardMedia
              component="img"
              alt="green iguana"
              height="140"
              image="/static/images/cards/contemplative-reptile.jpg"
            />
            <CardContent>
              <Typography
                gutterBottom
                variant="h5"
                component="div"
                sx={{ textAlign: "left" }}
              >
                Lizard
              </Typography>
              <Typography
                variant="body2"
                color="text.secondary"
                sx={{ textAlign: "right" }}
              >
                1200
              </Typography>
            </CardContent>
            <CardActions>
              <Button size="small">Buy</Button>
            </CardActions>
          </Card>
        </Grid>
      </Container>
    </Box>
  );
}
