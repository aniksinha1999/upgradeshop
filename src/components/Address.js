import {
  Box,
  Container,
  Grid,
  Select,
  TextField,
  Typography,
  Button,
} from "@mui/material";
import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function Address() {
  const [name, setName] = useState("");
  const [contactNumber, setContactNumber] = useState("");
  const [city, setCity] = useState("");
  const [landMark, setLandMark] = useState("");
  const [street, setStreet] = useState("");
  const [state, setState] = useState("");
  const [zipcode, setZipCode] = useState("");
  const [user, setUser] = useState("");

  const onChangeName = (name) => {
    setName(name);
  };
  const onChangeContactNumber = (contactNumber) => {
    setContactNumber(contactNumber);
  };
  const onChangeCity = (city) => {
    setCity(city);
  };
  const onChangeLandMark = (landMark) => {
    setLandMark(landMark);
  };
  const onChangeStreet = (street) => {
    setStreet(street);
  };
  const onChangeState = (state) => {
    setState(state);
  };
  const onChangeZip = (zipcode) => {
    setZipCode(zipcode);
  };
  const navigate = useNavigate();
  function submitAddress() {
    axios
      .post(
        "http://localhost:8080/api/addresses",
        {
          name: name,
          contactNumber: contactNumber,
          city: city,
          landmark: landMark,
          street: street,
          state: state,
          zipcode: zipcode,
          user: localStorage.getItem("user"),
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      )
      .then((res) => {
        localStorage.setItem("address_id", res.data);
        console.log(res.data);
        navigate("/summary");
      });
  }

  return (
    <Box>
      <Grid lg={12} xs={6}>
        <Typography
          variant="h5"
          sx={{ ml: 4, mr: 1, textAlign: "relative" }}
          gutterBottom
        >
          Select address
        </Typography>
        <Select
          required
          id="outlined-required"
          label="Select Address"
          defaultValue=""
          sx={{ width: "30%", height: "8%", mt: 1 }}
        />
      </Grid>

      <Container fixed sx={{ mt: 5, mrl: 1 }}>
        <hr></hr>
        <Typography variant="h4" sx={{ ml: 4, mr: 1 }} gutterBottom>
          Add address
        </Typography>
        <Grid lg={12} xs={6}>
          <TextField
            required
            id="outlined-required"
            label="Name"
            value={name}
            sx={{ width: "30%", mt: 1 }}
            onChange={(e) => onChangeName(e.target.value)}
          />
        </Grid>
        <Grid lg={12} xs={6}>
          <TextField
            required
            id="outlined-required"
            label="Contact"
            value={contactNumber}
            sx={{ width: "30%", height: "8%", mt: 1 }}
            onChange={(e) => onChangeContactNumber(e.target.value)}
          />
        </Grid>
        <Grid lg={12} xs={6}>
          <TextField
            required
            id="outlined-required"
            label="City"
            value={city}
            sx={{ width: "30%", height: "8%", mt: 1 }}
            onChange={(e) => onChangeCity(e.target.value)}
          />
        </Grid>
        <Grid lg={12} xs={6}>
          <TextField
            required
            id="outlined-required"
            label="LandMark"
            value={landMark}
            sx={{ width: "30%", height: "8%", mt: 1 }}
            onChange={(e) => onChangeLandMark(e.target.value)}
          />
        </Grid>
        <Grid lg={12} xs={6}>
          <TextField
            required
            id="outlined-required"
            label="Street"
            value={street}
            sx={{ width: "30%", height: "8%", mt: 1 }}
            onChange={(e) => onChangeStreet(e.target.value)}
          />
        </Grid>
        <Grid lg={12} xs={6}>
          <TextField
            required
            id="outlined-required"
            label="State"
            value={state}
            sx={{ width: "30%", height: "8%", mt: 1 }}
            onChange={(e) => onChangeState(e.target.value)}
          />
        </Grid>
        <Grid lg={12} xs={6}>
          <TextField
            required
            id="outlined-required"
            label="Zip Code"
            value={zipcode}
            sx={{ width: "30%", height: "8%", mt: 1 }}
            onChange={(e) => onChangeZip(e.target.value)}
          />
        </Grid>
        <Grid lg={12} md={12}>
          <Button
            variant="contained"
            sx={{ backgroundColor: "#3f51b5", width: "30%", mt: 3 }}
            onClick={submitAddress}
          >
            <b>Save Address</b>
          </Button>
        </Grid>
      </Container>
    </Box>
  );
}
