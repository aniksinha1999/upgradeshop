import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Unstable_Grid2";
import * as React from "react";
import Select from "@mui/material/Select";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import MenuItem from "@mui/material/MenuItem";
import { useEffect } from "react";
import axios from "axios";
import DeleteIcon from "@mui/icons-material/Delete";
import { ForkLeft } from "@mui/icons-material";
import EditIcon from "@mui/icons-material/Edit";

export default function ProductPage() {
  const [alignment, setAlignment] = React.useState("web");

  const handleChange = (event, newAlignment) => {
    setAlignment(newAlignment);
  };
  const [age, setAge] = React.useState("");
  const handleSelect = (event) => {
    setAge(event.target.value);
  };
  const [productsList, setProductList] = React.useState([]);
  const [isAdmin, setAdmin] = React.useState(false);

  useEffect(() => {
    axios
      .get("http://localhost:8080/api/products")
      .then((res) => setProductList(res.data));
  }, []);
  if (localStorage.getItem("role") === "ADMIN") {
    setAdmin(true);
  }

  return (
    <Box>
      <Grid lg={12} sm={6}>
        <ToggleButtonGroup
          color="primary"
          value={alignment}
          exclusive
          onChange={handleChange}
          aria-label="Platform"
          sx={{ mt: 5 }}
        >
          <ToggleButton value="All">All</ToggleButton>
          <ToggleButton value="Apparel">Apparel</ToggleButton>
          <ToggleButton value="Electronics">Electronics</ToggleButton>
          <ToggleButton value="Personalcare">Personal Care</ToggleButton>
        </ToggleButtonGroup>
      </Grid>
      {productsList &&
        productsList.map((product) => (
          <Grid lg={4} sm={3}>
            <Card sx={{ maxWidth: 345, mt: 8, ml: 3 }}>
              <CardMedia
                component="img"
                alt="green iguana"
                height="140"
                image={product.imageUrl}
              />
              <CardContent>
                <Typography
                  gutterBottom
                  variant="h5"
                  component="div"
                  sx={{ textAlign: "left" }}
                >
                  {product.name}
                </Typography>
                <Typography
                  variant="body2"
                  color="text.secondary"
                  sx={{ textAlign: "right" }}
                >
                  {product.price}
                </Typography>
              </CardContent>
              <CardActions sx={{ justifyContent: "space-between" }}>
                <a href="/orderprocess">
                  {" "}
                  <Button size="small">Buy</Button>
                </a>
                {isAdmin && (
                  <>
                    <EditIcon
                      sx={{ color: "black", justifyContent: "space-between" }}
                    />
                    <DeleteIcon sx={{ mr: 5 }} />
                  </>
                )}
              </CardActions>
            </Card>
          </Grid>
        ))}
    </Box>
  );
}
