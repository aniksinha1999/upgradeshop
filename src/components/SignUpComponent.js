import { Container } from "@mui/material";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Unstable_Grid2";
import LockPersonOutlinedIcon from "@mui/icons-material/LockPersonOutlined";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

import { useState } from "react";

import { Link } from "react-router-dom";
import axios from "axios";
export default function SignUpComponent() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [contact, setContact] = useState("");
  const [confirmPassword, setCofirmPassword] = useState("");
  const [msg, setMsg] = useState("");

  const onChangeFirstName = (firstName) => {
    setFirstName(firstName);
  };
  const onChangeLastName = (lastName) => {
    setLastName(lastName);
  };
  const onChangeEmail = (email) => {
    setEmail(email);
  };
  const onChangePassword = (password) => {
    setPassword(password);
  };
  const onChangeConfirmPassword = (confirmPassword) => {
    setCofirmPassword(confirmPassword);
  };
  const onChangeContact = (contact) => {
    setContact(contact);
  };
  const sendCustomerData = () => {
    if (password === confirmPassword) {
      axios
        .post("http://localhost:8080/api/auth/signup", {
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: password,
          contactNumber: contact,
          role: ["admin"],
        })
        .then((res) => {
          console.log(res.data);
        });
    }
  };

  // Your other middleware and route handlers here

  // Start the server

  return (
    <Box component="section" sx={{ p: 2, border: "InactiveBorder", mt: 1 }}>
      <Container fixed>
        <Grid container spacing={2}>
          <Grid lg={11} md={12}>
            <LockPersonOutlinedIcon
              color="primary"
              sx={{
                borderRadius: "50%",
                borderColor: "wheat",
                color: "pink",
                ml: 6,
              }}
            />
            <Typography variant="h4" sx={{ ml: 6 }} gutterBottom>
              Sign Up
            </Typography>
          </Grid>

          <Grid lg={12} md={12}>
            <TextField
              required
              id="outlined-required"
              label="First Name"
              sx={{ width: "40%", height: "10%", mt: 1 }}
              onChange={(e) => onChangeFirstName(e.target.value)}
              value={firstName}
            />
          </Grid>
          <Grid lg={12} md={12}>
            <TextField
              required
              id="outlined-required"
              label="Last Name"
              sx={{ width: "40%", height: "10%", mt: 1 }}
              onChange={(e) => onChangeLastName(e.target.value)}
              value={lastName}
            />
          </Grid>
          <Grid lg={12} md={12}>
            <TextField
              required
              id="outlined-required"
              label="Email"
              defaultValue=""
              value={email}
              sx={{ width: "40%", height: "10%", mt: 1 }}
              onChange={(e) => onChangeEmail(e.target.value)}
            />
          </Grid>
          <Grid lg={12} md={12}>
            <TextField
              required
              id="outlined-required"
              label="Password"
              value={password}
              sx={{ width: "40%", height: "10%", mt: 1 }}
              onChange={(e) => onChangePassword(e.target.value)}
            />
          </Grid>
          <Grid lg={12} md={12}>
            <TextField
              required
              id="outlined-required"
              label="Confirm Password"
              defaultValue=""
              value={confirmPassword}
              sx={{ width: "40%", height: "10%", mt: 1 }}
              onChange={(e) => onChangeConfirmPassword(e.target.value)}
            />
          </Grid>
          <Grid lg={12} md={12}>
            <TextField
              required
              id="outlined-required"
              label="Contact"
              value={contact}
              sx={{ width: "40%", height: "10%", mt: 1 }}
              onChange={(e) => onChangeContact(e.target.value)}
            />
          </Grid>
          <Grid lg={12} md={12}>
            <Button
              variant="contained"
              sx={{ backgroundColor: "#3f51b5", width: "40%", mt: 3 }}
              onClick={sendCustomerData}
            >
              <b>Sign In</b>
            </Button>
          </Grid>

          <Grid lg={12} md={12} sx={{ ml: 25 }}>
            <Link to="/login" style={{ textDecoration: "none", color: "blue" }}>
              Already have account? Sign In
            </Link>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}
