import { Container } from "@mui/material";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Unstable_Grid2";
import LockPersonOutlinedIcon from "@mui/icons-material/LockPersonOutlined";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import ProductPage from "./ProductsPage";

export default function LoginComponent() {
  const [email, setEmail] = useState("");

  const [password, setPassword] = useState("");
  const history = useNavigate();

  const onEmailChange = (email) => {
    setEmail(email);
  };
  const onPasswordChange = (password) => {
    setPassword(password);
  };

  const onSubmit = () => {
    axios
      .post("http://localhost:8080/api/auth/signin", {
        username: email,
        password: password,
      })
      .then((res) => {
        localStorage.setItem("token", res.data.token);
        localStorage.setItem("role", res.data.role[0].name);
        localStorage.setItem("user", res.data.user);
        localStorage.setItem("loggedIn", "loggedIn");
        history("/product");
        window.location.reload();
      });
  };

  return (
    <Box component="section" sx={{ p: 2, border: "InactiveBorder", mt: 12 }}>
      <Container fixed>
        <Grid container spacing={2}>
          <Grid lg={11} md={12}>
            <LockPersonOutlinedIcon
              color="primary"
              sx={{
                borderRadius: "50%",
                borderColor: "wheat",
                color: "pink",
                ml: 5,
              }}
            />
            <Typography variant="h4" sx={{ ml: 4 }} gutterBottom>
              Sign In
            </Typography>
          </Grid>
          <Grid lg={12} md={12}>
            <TextField
              required
              id="outlined-required"
              label="Email Address"
              sx={{ width: "40%", height: "10%", mt: 2 }}
              onChange={(e) => onEmailChange(e.target.value)}
              value={email}
            />
          </Grid>
          <Grid lg={12} md={12}>
            <TextField
              required
              id="outlined-required"
              label="Password"
              defaultValue=""
              value={password}
              sx={{ width: "40%", height: "10%", mt: 1 }}
              onChange={(e) => onPasswordChange(e.target.value)}
            />
          </Grid>
          <Grid lg={12} md={12}>
            <Button
              variant="contained"
              sx={{ backgroundColor: "#3f51b5", width: "40%", mt: 3 }}
              onClick={onSubmit}
            >
              <b>Sign In</b>
            </Button>
          </Grid>
          <Grid lg={9.5} md={12} sx={{ mt: 2 }}>
            <Link
              to="/signup"
              style={{ textDecoration: "none", color: "blue" }}
            >
              Don't have an account? Sign Up
            </Link>
          </Grid>
        </Grid>
        <Grid sx={{ mt: 6 }}>
          <h6>
            CopyRight <a href="/signup">@upGrad</a> 2021
          </h6>
        </Grid>
      </Container>
    </Box>
  );
}
