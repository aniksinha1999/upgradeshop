import { Box, Grid, Container, TextField, Button } from "@mui/material";
import axios from "axios";
import { useState } from "react";

export default function CreateProductForAdmin() {
  const [productName, setProductName] = useState("");
  const [category, setCategory] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [manufacturer, setManufacaturer] = useState("");
  const [imageurl, setImageUrl] = useState("");
  const [avaiableItems, setAvailableItems] = useState("");

  const onChangeProductName = (productName) => {
    setProductName(productName);
  };
  const onChangeCategory = (category) => {
    setCategory(category);
  };
  const onChangePrice = (price) => {
    setPrice(price);
  };
  const onChangeDescription = (description) => {
    setDescription(description);
  };
  const onChangeManufacturer = (manufacturer) => {
    setManufacaturer(manufacturer);
  };
  const onChangeImageUrl = (imageurl) => {
    setImageUrl(imageurl);
  };
  const onChangeAvailabelItems = (avaiableItems) => {
    setAvailableItems(avaiableItems);
  };

  const addProduct = () => {
    axios.post(
      "http://localhost:8080/api/products",
      {
        name: productName,
        category: category,
        price: price,
        description: description,
        manufacturer: manufacturer,
        imageurl: imageurl,
        availableItems: avaiableItems,
      },
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
  };

  return (
    <Box>
      <Container fixed sx={{ mt: 5 }}>
        <Grid item xs={12} lg={6}>
          <TextField
            required
            id="outlined-required"
            label="Product Name "
            sx={{ width: "40%", height: "10%", mt: 2 }}
            onChange={(e) => onChangeProductName(e.target.value)}
            value={productName}
          />
        </Grid>
        <Grid item xs={12} lg={6}>
          <TextField
            required
            id="outlined-required"
            label="Category"
            sx={{ width: "40%", height: "10%", mt: 2 }}
            onChange={(e) => onChangeCategory(e.target.value)}
            value={category}
          />
        </Grid>
        <Grid item xs={12} lg={6}>
          <TextField
            required
            id="outlined-required"
            label="Price"
            sx={{ width: "40%", height: "10%", mt: 2 }}
            onChange={(e) => onChangePrice(e.target.value)}
            value={price}
          />
        </Grid>
        <Grid item xs={12} lg={6}>
          <TextField
            required
            id="outlined-required"
            label="Description"
            sx={{ width: "40%", height: "10%", mt: 2 }}
            onChange={(e) => onChangeDescription(e.target.value)}
            value={description}
          />
        </Grid>
        <Grid item xs={12} lg={6}>
          <TextField
            required
            id="outlined-required"
            label="Manufacturer"
            sx={{ width: "40%", height: "10%", mt: 2 }}
            onChange={(e) => onChangeManufacturer(e.target.value)}
            value={manufacturer}
          />
        </Grid>
        <Grid item xs={12} lg={6}>
          <TextField
            required
            id="outlined-required"
            label="Image Url"
            sx={{ width: "40%", height: "10%", mt: 2 }}
            onChange={(e) => onChangeImageUrl(e.target.value)}
            value={imageurl}
          />
        </Grid>
        <Grid item xs={12} lg={6}>
          <TextField
            required
            id="outlined-required"
            label="Available Items"
            sx={{ width: "40%", height: "10%", mt: 2 }}
            onChange={(e) => onChangeAvailabelItems(e.target.value)}
            value={avaiableItems}
          />
        </Grid>
        <Grid lg={12} md={12}>
          <Button
            variant="contained"
            sx={{ backgroundColor: "#3f51b5", width: "30%", mt: 3 }}
            onClick={addProduct}
          >
            <b>Add Product</b>
          </Button>
        </Grid>
      </Container>
    </Box>
  );
}
