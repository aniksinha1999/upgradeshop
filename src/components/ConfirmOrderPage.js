import {
  Box,
  Container,
  Typography,
  Grid,
  Column,
  Divider,
} from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";

export default function ConfirmOrderPage() {
  const [address, setAddress] = useState(null);
  useEffect(() => {
    axios
      .get(
        `http://localhost:8080/api/addresses/${localStorage.getItem(
          "address_id"
        )}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      )
      .then((res) => setAddress(res.data));
  }, []);
  console.log("adddd");

  return (
    <Box>
      <Container
        fixed
        sx={{ bgcolor: "whitesmoke", borderRadius: "3px", mt: 5 }}
      >
        <Grid container spacing={2}>
          <Grid item xs={12} lg={6}>
            <Typography variant="h5">Order details</Typography>
          </Grid>
          <Divider sx={{ marginY: 2, color: "black" }} />
          <Grid item xs={12} lg={6}>
            <Typography variant="h5" sx={{ marginBottom: 1 }}>
              Address details
            </Typography>
            {address && (
              <Grid sx={{ textAlign: "left", ml: 25 }}>
                <p>
                  <strong>Name:</strong> {address.name}
                </p>
                <p>
                  <strong>Contact Number:</strong> {address.contactNumber}
                </p>
                <p>
                  <strong>Street:</strong> {address.street}
                </p>
                <p>
                  <strong>City:</strong> {address.city}
                </p>
                <p>
                  <strong>State:</strong> {address.state}
                </p>
                <p>
                  <strong>Zipcode:</strong> {address.zipcode}
                </p>
                <p>
                  <strong>Landmark:</strong> {address.landmark}
                </p>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}
