import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { Button } from "@mui/material";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));
const CenteredSearch = styled("div")({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  width: "100%",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

export default function Navbar() {
  const [login, setLogin] = useState(false);
  const [msg, setMsg] = useState("");

  const [isAdmin, setAdmin] = useState(false);
  useEffect(() => {
    // Check if user is logged in on component mount
    if (localStorage.getItem("loggedIn")) {
      setLogin(true);
      setMsg("Logout");

      if (localStorage.getItem("role") === "ADMIN") {
        setAdmin(true);
      }
    }
  }, []);
  const navigate = useNavigate();
  const handleLogin = () => {
    if (login) {
      // If already logged in, perform logout
      localStorage.removeItem("loggedIn");
      setLogin(false);
      setMsg("Login");
      localStorage.clear();
      navigate("/login");
    }
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar sx={{ backgroundColor: "#3f51b5" }} position="static">
        <Toolbar>
          <ShoppingCartIcon
            sx={{ color: "wheat", mt: 1, mr: 1 }}
          ></ShoppingCartIcon>
          <Typography
            variant="h5"
            noWrap
            component="div"
            sx={{
              flexGrow: 1,
              display: { xs: "none", sm: "block" },
              mt: 1,
              textAlign: "left",
            }}
          >
            Upgrad E-Shop
          </Typography>

          <Search sx={{ mr: 47 }}>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ "aria-label": "search" }}
            />
          </Search>
          <Typography>Home</Typography>
          {isAdmin && (
            <a href="/addProduct">
              <Typography sx={{ ml: 6 }}>Add Product</Typography>
            </a>
          )}
          {/* // Render the button only if user is logged in */}
          {login && (
            <Button
              variant="contained"
              size="medium"
              sx={{ ml: 4 }}
              onClick={handleLogin}
            >
              {msg}
            </Button>
          )}
        </Toolbar>
      </AppBar>
    </Box>
  );
}
